// Contient la liste des albums et leur titre
const albums = [
  {
    "albumImg" : "https://images.hhv.de/catalog/shop_entry_popup_big/00609/609604_1.jpg",
    "albumName" : 'Mon pays c\'est l\'amour',
    "albumDate" : '19/10/2018',
    "albumTitles" : [
      {
        "titleName" : 'J\'en parlerai au diable',
        "titleDuration" : "2'10",
      },
      {
        "titleName" : 'Mon pays c\'est l\'amour',
        "titleDuration" : "1'25",
      },
      {
        "titleName" : 'Made in Rock\'n\'Roll',
        "titleDuration" : "2'10",
      },
      {
        "titleName" : 'Pardonne-moi',
        "titleDuration" : "1'25",
      },
      {
        "titleName" : 'Interlude',
        "titleDuration" : "2'10",
      },
      {
        "titleName" : '4 m²',
        "titleDuration" : "1'25",
      },
      {
        "titleName" : 'Back in LA',
        "titleDuration" : "2'10",
      },
      {
        "titleName" : 'L\'Amérique de William',
        "titleDuration" : "1'25",
      },
      {
        "titleName" : 'Un enfant du siècle',
        "titleDuration" : "2'10",
      },
      {
        "titleName" : 'Tomber encore',
        "titleDuration" : "1'25",
      },
      {
        "titleName" : 'Je ne suis qu\'un homme',
        "titleDuration" : "1'25",
      },
    ]
  },
  {
    "albumImg" : "http://images.music-story.com/img/album_J/johnny-hallyday-lorada.jpg",
    "albumName" : 'Lorada',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Lorada',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Est-Ce Que Tu Me Veux Encore',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Rester Libre',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Le Regard Des Autres',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Lady Lucille',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Un Rêve à Faire',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'J\'La Croise Tout Les Matins',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Chercher Les Anges',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Tout Feu Toute Femme',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Quand Le Masque Tombe',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Ami',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Aime Moi',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Ne M\'Oublie Pas',
        "titleDuration" : "3'15",
      },
    ]
  },
  {
    "albumImg" : "http://images.music-story.com/img/album_J/johnny-hallyday-un-soir-a-l-olympia.jpg",
    "albumName" : 'Album2',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Titre1',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Titre2',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://static.fnac-static.com/multimedia/Images/FR/NR/72/e8/5f/6285426/1540-1/tsp20140912100119/Rester-vivant-Edition-limitee-CD-DVD.jpg",
    "albumName" : 'Rester vivant',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'J\'ai ce que j\'ai donné',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Regarde-nous',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Rester vivant',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Seul',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Au café de l\'avenir',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Une lettre à l\'enfant',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Jt\'ai même pas dit',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Si j\'avais su la vie',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'On s\'habitue à tout',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Te manquer',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Te voir grandir',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'A nos promesses',
        "titleDuration" : "1'10",
      },
    ]
  },
  {
    "albumImg" : "https://i.ytimg.com/vi/bFnrXXLGfWc/maxresdefault.jpg",
    "albumName" : 'Album2',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Titre1',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Titre2',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://img.discogs.com/DbaxyIHQyZ0cXw58c-qOaaY7mvY=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-6809031-1427064298-8105.jpeg.jpg",
    "albumName" : 'A la vie a la mort',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Entre nous',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Dis le moi',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://upload.wikimedia.org/wikipedia/en/2/23/Sang_pour_sang.jpg",
    "albumName" : 'Sang pour Sang',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Sang pour Sang',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Le poid de mes maux',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/71e1MXziVrL._SL1400_.jpg",
    "albumName" : 'Cadillac',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Les vautours',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Mirador',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://www.la-face-cachee.com/import_upload/images/2789321.png",
    "albumName" : 'Gang',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'L\'Envie',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je t\'attends',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/416KPVAXWNL.jpg",
    "albumName" : 'Hamlet',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Ouverture',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Prologue',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://m.media-amazon.com/images/I/81IDkjj5FJL._SS500_.jpg",
    "albumName" : 'Born Rocker Tour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Que je t\'aimes',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Allumer le feu',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/4/2/5/0731454696524/tsp20121014055320/Insolitudes.jpg",
    "albumName" : 'Insolitude',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'La musique que j\'aime',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Tu peux partir si tu le veux',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://p6.storage.canalblog.com/68/76/636073/67853111.jpg",
    "albumName" : 'Flagrant délit',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Flagrant délit',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fils de personne',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/71sFeDq4gWL._SL1400_.jpg",
    "albumName" : 'Pas facile',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'L\'Envie',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je t\'attends',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://www.la-face-cachee.com/import_upload/images/2789321.png",
    "albumName" : 'Derrière l\'amour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Rendez-vous en enfer',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Le Jour J, l\'Heure H',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://m.media-amazon.com/images/I/8191TakAVRL._SS500_.jpg",
    "albumName" : 'Flashback Tour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Intro',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je suis né dans la rue',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/5/0/6/0825646743605/tsp20120919104733/Jamais-seul.jpg",
    "albumName" : 'Jamais seul',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Paul et Mick',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Guitar Hero',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/91qVwWt2%2BHL._SL1500_.jpg",
    "albumName" : 'Quelque part un aigle',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'La caisse',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Sage pour vous',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "http://img.allformusic.fr/pochette/300/j/johnny-hallyday/la-peur.jpg",
    "albumName" : 'La peur',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'La peur',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Le survivant',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://img.cdandlp.com/2017/03/imgL/118658664.jpg",
    "albumName" : 'Rock\'n\'Roll Attitude',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'L\'Envie',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je t\'attends',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/819uYoN9l%2BL._SL1200_.jpg",
    "albumName" : 'Rough Town',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://images-na.ssl-images-amazon.com/images/I/81N0JA3gh%2BL._SL1423_.jpg",
    "albumName" : 'Flashback Tour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Intro',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je suis né dans la rue',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/5/2/0/0044007721025/tsp20121001085017/Bercy-92.jpg",
    "albumName" : 'Flashback Tour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Intro',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je suis né dans la rue',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
  {
    "albumImg" : "https://i.ytimg.com/vi/nrZ4sEPKBp4/maxresdefault.jpg",
    "albumName" : 'Flashback Tour',
    "albumDate" : '01/02/2020',
    "albumTitles" : [
      {
        "titleName" : 'Intro',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Fool for the blues',
        "titleDuration" : "3'15",
      },
      {
        "titleName" : 'Je suis né dans la rue',
        "titleDuration" : "1'10",
      },
      {
        "titleName" : 'Love Affair',
        "titleDuration" : "1'10",
      }
    ]
  },
]

// contient la source des différentes images
const pictures = [
  "https://i.ytimg.com/vi/bFnrXXLGfWc/maxresdefault.jpg",
  "https://medias.laprovence.com/RrK9wTvavV6op2QWq3ogxeYNkTQ=/0x123:1800x1116/850x575/top/smart/1ce7e14389574d71b78cc24ebf4959c4/part-par-par6174262-1-1-0.jpg",
  "https://remeng.rosselcdn.net/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2019/11/07/node_106854/11418743/public/2019/11/07/B9721510436Z.1_20191107104056_000%2BGBEERSG86.1-0.jpg",
  "https://cdn-europe1.lanmedia.fr/var/europe1/storage/images/europe1/culture/une-nouvelle-version-dun-titre-emblematique-de-johnny-hallyday-devoile-3923244/53571136-1-fre-FR/Une-nouvelle-version-d-un-titre-emblematique-de-Johnny-Hallyday-devoile.jpg",
  "https://societyofrock.com/wp-content/uploads/2015/11/kirk-hammett-735x413.jpg",
  "https://cdn.mos.cms.futurecdn.net/nLWKWRu2Y8h5v8LNv8HDdW.jpg",
  "https://image.businessinsider.com/50e58d9b6bb3f7fa12000018?width=1100&format=jpeg&auto=webp.jpg",
  "https://www.guitarnoise.com/images/features/gary-moore-live-1024x585.jpg",
  "https://remeng.rosselcdn.net/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2019/08/23/node_88231/10990572/public/2019/08/23/B9720658371Z.1_20190823142752_000%2BGSSE9VJ5U.1-0.jpg",
  "http://s2.favim.com/orig/37/a7x-avenged-sevenfold-syn-gates-synyster-gates-Favim.com-306075.jpg",
  "https://www.revolvermag.com/sites/default/files/styles/image_750_x_420/public/media/images/article/synyster-gates-avengedsevenfold-gettyimages-647264106.jpg",
  "https://i.skyrock.net/7202/30497202/pics/1058735268_small_41.jpg",
  "https://www.guitarspeed99.com/images/artistes/slash-134832.jpg",
  "https://resize.programme-television.ladmedia.fr/r/670,670/img/var/premiere/storage/images/tele-7-jours/news-tv/louis-bertignac-je-voulais-faire-un-genre-de-the-voice-avec-des-groupes-4633182/95430539-1-fre-FR/Louis-Bertignac-Je-voulais-faire-un-genre-de-The-Voice-avec-des-groupes.jpg",
  "https://www.billboard.com/files/styles/article_main_image/public/media/jimi-hendrix-feb-1969-live-billboard-1548.jpg",
]

const users = [
  {
    "email" : 'alexis.dayne@gmail.com',
    "password" : 'test',
  },
  {
    "email" : 'pro.lsmb@@gmail.com',
    "password" : 'test',
  },
  {
    "email" : 'test@test.test',
    "password" : 'test',
  },
]

export default {
  albums: albums,
  pictures : pictures,
  users : users,
}
