import React from "react";
import Navigation from './component/navigation';
import Data from './asset/data';
import "./App.css";
import {Link} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Navigation></Navigation>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
              <h5>Biographie</h5>
              <span>
                <img src="img\johnny01.jpg" alt="TEST" width="250px" class="float-left mr-1 mb-1"></img>
                Jean-Philippe Smet naît à Paris le 15 juin 1943. Le divorce de ses parents (une mère française, un père belge) l'oblige à suivre Hélène Mar, la soeur de son père et ses deux cousines Desda et Menen. Le jeune adolescent évolue alors dans le monde du cabaret. Il interprète de temps en temps un numéro dans le spectacle familial. Il trimballe ses valoches de Londres à Copenhague, apprenant son métier de saltimbanque. De retour à Paris, il découvre un courant musical en pleine explosion, le rock'n'roll. Il n'est alors rien qu'un petit guitariste franco-belge, fan d'une nouvelle idole, Elvis Presley.
              </span>
              <br/>
                <Link to="/biographie">Voir plus...</Link>
            </div>
          </div>
          <div class="carousel-item">
            <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
              <h5>Discographie</h5>
              <div class="albums">
              {
                Data.albums.slice(0,12).map((album, i) => {
                  return (
                    <div key={i} class="col-lg-3 col-md-4 col-sm-6 col-xs-12 album" id={"album-" + i}>
                      <img class="album-img img-thumbnail img-size1"
                      src={album.albumImg.startsWith('http') ? album.albumImg : '/asset/img/' + album.albumImg}
                      alt={album.albumName}></img>
                    </div>
                  )
                })
              }
              </div>
              <br/>
              <Link to="/discographie" class="col-2 offset-5">Liste des albums</Link>
            </div>
          </div>
          <div class="carousel-item">
            <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
              <h5>Photos</h5>
              <div class="pictures overflow-auto">
              {
                Data.pictures.slice(0,12).map((picture, i) => {
                  return (
                    <div key={i} class="col-lg-3 col-md-4 col-sm-6 col-xs-12 picture" id={"picture-" + i}>
                      <img class="picture-img img-size1 img-thumbnail"
                      src={picture.startsWith('http') ? picture : '/asset/img/' + picture}
                      alt={picture}></img>
                    </div>
                  )
                })
              }
              </div>
              <br/>
              <Link class="col-2 offset-5" to="/photos">Liste des photos</Link> 
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      {/* 
      <div class="container-fluid">
        <div class="row text-left">
          <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
            <h5>Biographie</h5>
            <span>
              <img src="img\johnny01.jpg" alt="TEST" width="250px" class="float-left mr-1 mb-1"></img>
              Jean-Philippe Smet naît à Paris le 15 juin 1943. Le divorce de ses parents (une mère française, un père belge) l'oblige à suivre Hélène Mar, la soeur de son père et ses deux cousines Desda et Menen. Le jeune adolescent évolue alors dans le monde du cabaret. Il interprète de temps en temps un numéro dans le spectacle familial. Il trimballe ses valoches de Londres à Copenhague, apprenant son métier de saltimbanque. De retour à Paris, il découvre un courant musical en pleine explosion, le rock'n'roll. Il n'est alors rien qu'un petit guitariste franco-belge, fan d'une nouvelle idole, Elvis Presley.
            </span>
            <br/>
            <Link to="/biographie">Voir plus...</Link>
          </div>
          <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
            <h5>Discographie</h5>
            <div class="albums">
            {
              Data.albums.slice(0,10).map((album, i) => {
                return (
                  <div key={i} class="col-lg-3 col-md-4 col-sm-6 col-xs-12 album" id={"album-" + i}>
                    <img class="album-img img-fluid"
                    src={album.albumImg.startsWith('http') ? album.albumImg : '/asset/img/' + album.albumImg}
                    alt={album.albumName}></img>
                  </div>
                )
              })
            }
            </div>
            <br/>
            <Link to="/discographie">Voir plus...</Link>
          </div>
          <div class="col-sm shadow p-3 mx-3 my-3 rounded home-containers">
            <h5>Photos</h5>
            <div class="pictures">
            {
              Data.pictures.slice(0,10).map((picture, i) => {
                return (
                  <div key={i} class="col-lg-3 col-md-4 col-sm-6 col-xs-12 picture" id={"picture-" + i}>
                    <img class="picture-img img-fluid"
                    src={picture.startsWith('http') ? picture : '/asset/img/' + picture}
                    alt={picture}></img>
                  </div>
                )
              })
            }
            </div>
            <br/>
            <Link to="/photos">Voir plus...</Link> 
          </div>
        </div>
      </div> */}
    </div>
  );
}

export default App;
