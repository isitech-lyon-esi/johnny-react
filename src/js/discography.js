import React, { Component } from 'react';
import Navigation from '../component/navigation';
import Data from '../asset/data';
import '../css/discography.css';

class Discography extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      changed: true,
      search : '',
      data : [],
    }
  }
  handleChange(e) {
    e.persist();
    let query = e.target.value
    this.setState(state => state[e.target.id] = query);
    this.setState( (state) => state.data = Data.albums.filter(album => album.albumName.toLowerCase().includes(query.toLowerCase())) );
  }
  componentWillReceiveProps(newprops){
    //generalement utilisé pour verifier les props recues par le composant
  }
  componentDidMount() {
    //generalement utilisé pour les appels réseaux
  }
  componentWillUnmount() {
    //generalement utiliser pour supprimer les timers
  }

  render() {
    let albums = [];
    if(this.state.search && this.state.search !== '') {
      if (this.state.data.length > 0) {
        albums = this.state.data;
      } else {
        // Pas d'album corresopndant
      }
    } else {
      albums = Data.albums;
    }
    console.log(albums);
    return (
        <div class="App discography-page">
          <Navigation></Navigation>
          <div class="input-group">
            <input id="search" value={this.state.search} onChange={this.handleChange.bind(this)}
            type="text" class="form-control mx-4 my-2" placeholder="Nom de l'album" aria-label="Recherche" aria-describedby="basic-addon1"/>
          </div>
          <div class="albums row p-2 m-1">
            { albums.length > 0 ?
              albums.map((album, i) => {
                return (
                  <div key={i} class="col-lg-3 col-md-4 col-sm-6 col-xs-12 hovereffect album" id={"album-" + i}>
                    <img class="album-img img-size"
                    src={album.albumImg.startsWith('http') ? album.albumImg : '/asset/img/' + album.albumImg}
                    alt={album.albumName}></img>
                    <div class="overlay overflow-auto overlay-transparent">
                      <h2 class="album-title-name">{album.albumName}</h2>
                      <ul class="titles">
                      {
                        album.albumTitles.map((title, j) => {
                          return (
                            <li key={j} class="title" id={"title-" +j}>{title.titleName}</li>
                          )
                        })
                      }
                      </ul>
                    </div>
                  </div>
                )
              })
              : <p>Pas de corespondance d'album</p>
            }
          </div>
      </div>
    );
  }
}

export default Discography;