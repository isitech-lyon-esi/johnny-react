import React, { Component } from 'react';
import Navigation from '../component/navigation';
import Data from '../asset/data';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      changed: true,
      loginPage: true,
      email :  "",
      mdp: "",
      mdp1: "",
      mdp2: "",
      alert: null,
    }
  }
  handleChange(e) {
    e.persist();
    console.log(e);
    this.setState(state => state[e.target.id] = e.target.value);
  }
  componentWillReceiveProps(newprops) {
    //generalement utilisé pour verifier les props recues par le composant
  }
  componentDidMount() {
    //generalement utilisé pour les appels réseaux
  }
  componentWillUnmount() {
    //generalement utiliser pour supprimer les timers
  }
  render() {

    return (
      <div class="App login-page">
        <Navigation></Navigation>
        <div class="container-fluid">
          <div class="row text-left">
            <div class="col-sm-6 offset-sm-3 p-3 shadow my-3 rounded home-containers">
              <div class="btn-group btn-group-toggle">
                <button className="btn btn-secondary" onClick={() => this.setState({loginPage: true})}> Se connecter </button>
                <button className="btn btn-secondary" onClick={() => this.setState({loginPage: false})}> S'inscire </button>
              </div>
              {this.state.loginPage ? this.loginForm() : this.signupForm()}
              {this.state.alert}
            </div>
          </div>
        </div>
      </div>
    );
  }
  togglePage() {
    this.setState(state => state.loginPage = !state.loginPage);
  }
  loginForm() {
    return (
      <form>
        <br/>
        <h5>Se connecter</h5>
        <div class="form-group">
          <label for="email">Adresse mail</label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp"></input>
        </div>
        <div class="form-group">
          <label for="mdp">Mot de passe</label>
          <input type="password" class="form-control" id="mdp"></input>
        </div>
        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="checkbox"></input>
          <label class="form-check-label" for="checkbox">Se souvenir</label>
        </div>
        <button class="btn btn-primary" onClick={this.login.bind(this)}>Se connecter</button>
      </form>
    );
  }
  signupForm() {
    return (
      <form>
        <br/>
        <h5>S'inscrire</h5>
        <div class="form-group">
          <label for="email">Adresse mail</label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp"></input>
        </div>
        <div class="form-group">
          <label for="mdp">Mot de passe</label>
          <input type="password" class="form-control" id="mdp1" value={this.state.mdp1}
          onChange={this.handleChange.bind(this)}></input>
        </div>
        <div class="form-group">
          <label for="mdp2">Retaper mot de passe</label>
          <input type="password" class="form-control" id="mdp2" value={this.state.mdp2}
          onChange={this.handleChange.bind(this)}></input>
          {this.verifyPassword()}
        </div>
        <button class="btn btn-primary" onClick={this.signup()}>S'inscrire</button>
      </form>
    );
  }

  login() {
    const users = Data.users;
    const result = users.find((user) => user.email === this.state.email && user.password === this.state.mdp);
    console.log(result);
    if (!result) {
      this.setState(state => state.alert = 
        <div class="alert alert-danger" role="alert">
          Les identifiants sont incorrectes
        </div>
      )
    } else {
      this.setState(state => state.alert = 
        <div class="alert alert-success" role="alert">
          Connexion réussie
        </div>
      )
    }
  }
  signup() {
    this.verifyPassword();
  }
  verifyPassword(){
    if(this.state.mdp2 === ''){
      return;
    }
    if(this.state.mdp2 !== this.state.mdp1){
      return(<div class="alert alert-danger" role="alert">
      Les mots de passe sont différents !
    </div>)
    } else {
      return(<div class="alert alert-success" role="alert">
      OK
    </div>);
    }
  }
  verifyEmail(){
    if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)){

    } else {
      return(<div class="alert alert-danger" role="alert">
      Email invalide
    </div>);
    }
  }
}
export default Login;