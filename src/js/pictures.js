import React, { Component } from 'react';
import Navigation from '../component/navigation';
import '../css/pictures.css';

import Data from '../asset/data';

class Pictures extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      changed: true
    }
  }
  componentWillReceiveProps(newprops){
    //generalement utilisé pour verifier les props recues par le composant
  }
  componentDidMount() {
    //generalement utilisé pour les appels réseaux
  }
  componentWillUnmount() {
    //generalement utiliser pour supprimer les timers
  }
  render() {
      // Rendu de la page login
    return (
      <div class="App pictures-page">
        <Navigation></Navigation>
        <section class="bg-dark" id="portfolio">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">GALERIE PHOTOS</h2>
              </div>
            </div>
            <div class="row">
              {
                Data.pictures.map( (picture, i) => {
                  return (
                    <div key={i} class="col-md-4 col-sm-6 portfolio-item">
                      <a class="portfolio-link" data-toggle="modal" href={"#portfolioModal"+ i}>
                        <div class="portfolio-hover">
                          <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                          </div>
                        </div>
                        <img class="img-fluid" src={picture} alt=""></img>
                      </a>
                      <div class="portfolio-caption">
                        <h4>Photo {i+1}</h4>
                        <p class="text-muted">Photo {i+1}</p>
                      </div>
                    </div>
                  );
                } )
              }
            </div>
          </div>
        </section>
{/*  Portfolio Modals  */}
        {
          Data.pictures.map( (picture,i) => {
            return(
              <div key={i} class="portfolio-modal modal fade" id={"portfolioModal"+ i} tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                        <div class="rl"></div>
                      </div>
                    </div>
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-8 mx-auto">
                          <div class="modal-body">
                            {/* Project Details Go Here  */}
                            <h2 class="text-uppercase">Photo {i + 1}</h2>
                            <img class="img-fluid d-block mx-auto" src={picture} alt=""></img>
                            <ul class="list-inline">
                              <p>Photo description</p>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })
        }   
      </div>
    );
  }
}
export default Pictures;