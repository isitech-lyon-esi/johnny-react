import React, { Component } from "react";
import { Link }from 'react-router-dom';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      changed: true
    }
  }
  componentWillReceiveProps(newprops){
    //generalement utilisé pour verifier les props recues par le composant
  }
  componentDidMount() {
    //generalement utilisé pour les appels réseaux
  }
  componentWillUnmount() {
    //generalement utiliser pour supprimer les timers
  }

  render() {
    return (
      <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
        <Link to="/" class="navbar-brand">
        </Link>
        <img src="img\coffin01.png" alt="TEST" width="50px"></img>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse container-fluid" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active float-left">
              <Link to="/" class="nav-link">
                Accueil<span class="sr-only">(current)</span>
              </Link>
            </li>
            <li class="nav-item float-left">
              <Link to="/discographie" class="nav-link">
                Discographie
              </Link>
            </li>
            <li class="nav-item float-left">
              <Link to="/biographie" class="nav-link">
                Biographie
              </Link>
            </li>
            <li class="nav-item float-left">
              <Link to="/photos" class="nav-link">
                Galerie de photos
              </Link>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="nav-item">
              <Link to="/login" class="nav-link">
                Connexion / Inscription
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navigation;