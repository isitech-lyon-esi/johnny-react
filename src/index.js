import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Biography from './js/biography';
import Discography from './js/discography';
import Login from './js/login';
import Pictures from './js/pictures';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import * as serviceWorker from './serviceWorker';

const Root = () => {
  return (
    <Router>
      <Switch>
          <Route exact path='/' component={App} />
          <Route exact path='/biographie' component={Biography} />
          <Route exact path='/discographie' component={Discography} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/photos' component={Pictures} />
        </Switch>
    </Router>
  )
}

ReactDOM.render(
  <Root />
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
